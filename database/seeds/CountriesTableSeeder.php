<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('countries')->insert([
            'name' => 'Nigeria'
        ]);

        DB::table('countries')->insert([
            'name' => 'Cameroun'
        ]);

        DB::table('countries')->insert([
            'name' => 'Spain'
        ]);
    }
}
